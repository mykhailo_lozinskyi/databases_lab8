﻿// See https://aka.ms/new-console-template for more information

using DB_Lab8.Models;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using NpgsqlTypes;
using StoredProcedureEFCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DB_Lab8
{
    public class Program
    {
        static void Main(string[] args)
        {
            using (var db = new ComputerCoursesDbContext())
            {
                string choicesString = "Select the desired operation: "
                                        + "\n1 - SELECT a person from the \"person\" table by first and last name"
                                        + "\n2 - UPDATE a record in the \"person\" table"
                                        + "\n3 - CALL the create_rating procedure"
                                        + "\n4 or any other number - Exit" 
                                        + "\nYour choice: ";
                int operationChoice = 0;
                while (true)
                {
                    operationChoice = GetUserChoice(choicesString);
                    switch (operationChoice)
                    {
                        case 1:
                            HandlePersonSelect(db);
                            break;
                        case 2:
                            HandlePersonUpdate(db);
                            break;
                        case 3:
                            HandleCreateRatingProcedureCall(db);
                            break;
                        default:
                            Console.WriteLine("\nThe program completed its execution successfully!");
                            return;
                    }
                }
            }
        }

        private static int GetUserChoice(string choiceString)
        {
            while (true)
            {
                Console.WriteLine(choiceString);
                try
                {
                    return int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid input. Please enter a number.\n".ToUpper());
                }
            }
        }

        private static float GetUserChoiceFloat(string choiceString)
        {
            while (true)
            {
                Console.WriteLine(choiceString);
                try
                {
                    return float.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid input. Please enter a valid floating-point number.\n".ToUpper());
                }
            }
        }

        private static void HandlePersonSelect(ComputerCoursesDbContext db)
        {
            Console.Write("Enter first name: ");
            string fName = Console.ReadLine();
            Console.Write("Enter last name: ");
            string lName = Console.ReadLine();

            string sql = $"SELECT * FROM person " +
                         $"WHERE first_name = '{fName}' " +
                         $"AND last_name = '{lName}'";
            Console.WriteLine($"\nThe generated sql query: \n{sql}\n");
            var persons = db.Persons.FromSqlRaw(sql).ToList();
            if (persons.Count == 0)
            {
                Console.WriteLine($"\nThere is no record for the user {fName} {lName} in the person table.\n");
                return;
            }
            var firstNames = persons.Select(p => p.FirstName).ToList();
            var lastNames = persons.Select(p => p.LastName).ToList();
            var usernames = persons.Select(p => p.Username).ToList();
            int maxFirstNameLength = FindMaxLength(firstNames, "First Name");
            int maxLastNameLength = FindMaxLength(lastNames, "Last Name");
            int maxUsernameLength = FindMaxLength(usernames, "Username");

            Console.WriteLine($"{"Id".PadRight(3)}" +
                     $"\t{"First Name".PadRight(maxFirstNameLength)}" +
                     $"\t{"Last Name".PadRight(maxLastNameLength)}" +
                     $"\t{"Username".PadRight(maxUsernameLength)}" +
                     $"\tBirth Date" +
                     $"\tEmail");

            foreach (var person in persons)
            {
                Console.WriteLine($"{person.PersonId.ToString().PadRight(3)}" +
                          $"\t{person.FirstName.PadRight(maxFirstNameLength)}" +
                          $"\t{person.LastName.PadRight(maxLastNameLength)}" +
                          $"\t{person.Username.PadRight(maxUsernameLength)}" +
                          $"\t{person.BirthDate}" +
                          $"\t{person.Email}");
            }
            Console.WriteLine();
        }

        private static void HandlePersonUpdate(ComputerCoursesDbContext db)
        {
            while (true)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    int updatePersonId = GetUserChoice("Enter person_id to update: ");
                    Console.Write("Enter first name: ");
                    string fName = Console.ReadLine();
                    Console.Write("Enter last name: ");
                    string lName = Console.ReadLine();
                    Console.Write("Enter username: ");
                    string username = Console.ReadLine();
                    try
                    {
                        string sql = $"UPDATE person " +
                                     $"SET first_name = '{fName}', " +
                                     $"last_name = '{lName}', " +
                                     $"username = '{username}' " +
                                     $"WHERE person_id = {updatePersonId}";
                        Console.WriteLine($"\nThe generated sql query: \n{sql}\n");
                        int rowsAffected = db.Database.ExecuteSqlRaw(sql);
                        transaction.Commit();
                        string resStr = (rowsAffected > 0) ?
                            $"\nThe record with person_id = {updatePersonId} was successfully updated.\n" :
                            $"\nThe record with person_id = {updatePersonId} does not exist in the database.\n";

                        Console.WriteLine(resStr);
                        break;
                    }
                    catch (PostgresException postgresException)
                    {
                        string errorStr = string.Empty;
                        switch (postgresException.SqlState)
                        {
                            case "23505":
                                errorStr = "Error: Duplicate key violation. This username is already in use.";
                                break;
                            case "23514":
                                errorStr = "Error: The user's first and last name must consist only of upper and lower case " +
                                    "letters of the English alphabet ([a-zA-Z]) and contain at least 1 character.";
                                break;
                        }
                        Console.WriteLine(errorStr);
                        Console.WriteLine($"Full Error Message: ");
                        Console.WriteLine($"Error: {postgresException.Message}");
                        transaction.Rollback();
                    }
                    catch (DbUpdateException ex)
                    {
                        Console.WriteLine($"Error: {ex.Message}");
                        transaction.Rollback();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error: {ex.Message}");
                    }
                    Console.WriteLine("\nPlease try to perform the operation again.");
                }
            }
        }

        private static void HandleCreateRatingProcedureCall(ComputerCoursesDbContext db)
        {
            while (true)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    int submissionId = GetUserChoice("Enter the submission_id to create the record in the rating table: ");
                    float originalGrade = GetUserChoiceFloat("Enter a grade for the submission: ");

                    try
                    {
                        NpgsqlParameter procedureMsg = new NpgsqlParameter("@procedure_mess", "")
                        {
                            Direction = ParameterDirection.InputOutput
                        };
                        string sql = $"CALL create_rating({submissionId}, {originalGrade}, @procedure_mess)";
                        Console.WriteLine($"\nThe generated sql query: \n{sql}\n");

                        var result = db.Database.ExecuteSqlRaw(sql, procedureMsg);
                        string resStr = "\nThe procedure create_rating was successfully completed. A corresponding record" +
                            " is created in the rating table.\n";

                        string procedureMessage = procedureMsg.NpgsqlValue.ToString();
                        if (procedureMessage == "OK")
                        {
                            transaction.Commit();
                            Console.WriteLine(resStr);
                            break;
                        }
                        else
                        {
                            transaction.Rollback();
                            resStr = "An error occurred while executing the create_rating procedure:\n" + procedureMessage;
                        }
                        Console.WriteLine(resStr);
                    }
                    catch (PostgresException postgresException)
                    {
                        Console.WriteLine($"Error: {postgresException.Message}");
                        transaction.Rollback();
                    }
                    catch (DbUpdateException ex)
                    {
                        Console.WriteLine($"Error: {ex.Message}");
                        transaction.Rollback();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error: {ex.Message}");
                    }
                    Console.WriteLine("\nPlease try to perform the operation again.");
                }
            }
        }

        private static int FindMaxLength(List<string> strings, string titleString)
        {
            int maxLength = strings.Max(s => s.Length);
            if (maxLength < titleString.Length)
            {
                maxLength = titleString.Length;
            }
            return maxLength;
        }
    }

}