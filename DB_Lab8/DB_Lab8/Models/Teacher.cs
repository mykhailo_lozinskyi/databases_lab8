﻿using System;
using System.Collections.Generic;

namespace DB_Lab8.Models;

public partial class Teacher
{
    public int TeacherId { get; set; }
    public virtual ICollection<Course> Courses { get; set; } = new List<Course>();

    public virtual Person TeacherNavigation { get; set; } = null!;
}


